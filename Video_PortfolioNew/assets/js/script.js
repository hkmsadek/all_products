(function($) {
'use strict';

    $(document).ready(function() {


 // side_menu Modal
      $('.main_logo').click(function() {
              $(".side_menu").addClass("side_menu_modal_show");
              $(".side_menu_overlay").addClass("side_menu_modal_fadeIn");
          });

          $(".side_menu_close").click(function(e) {
              e.preventDefault();
              $(".side_menu").removeClass("side_menu_modal_show");
              $(".side_menu_overlay").removeClass("side_menu_modal_fadeIn");
          });


// $(".single_videos_slider .col-md-12").hide();
//  $('.single_videos_slider .videos_btn').click(function() {
//  	$(".single_videos_slider .col-md-12").show(800);

//  });

//  $('.video_cross').click(function(){
//  	$('.single_videos_slider .col-md-12').hide(800);
//  });

	// $(".single_P_vlog_slider").owlCarousel({
	// 	items:1,
	// 	loop:true,
	// 	dots:false,
	// 	nav:true,
	// 	autoplay:false
	// });


	 //video popup
       $('.video-popup').click(function() {
          $(" .video_area").addClass("video_modal_show");
          $(".video_area_overlay").addClass("video_modal_fadeIn");
          $(".topbar_area,.menu_area").css("display","none");
      });
      $(".video_toggle").click(function(e) {
          e.preventDefault();
          $(".video_area").removeClass("video_modal_show");
          $(".video_area_overlay").removeClass("video_modal_fadeIn");
          $(".topbar_area,.menu_area").css("display","block");
      });


   });

    $(window).on('load', function() {
		$('#status').fadeOut();
		$('#preloader').delay(100).fadeOut('fast');
		$('body').delay(100).css({'overflow':'visible'});
		
		
		});


})(jQuery);