(function($) {
'use strict';

    $(document).ready(function() {


      //Mobile Menu
      $(".mobile_menu_area").hide();
      $(".mobile_toggle").on("click", function() {
        $(this).siblings(".mobile_menu_area").show();
      });
      $(".mobile_menu_close i").on("click", function() {
        $(this).parent().parent(".mobile_menu_area").hide();
      });



      // Reservation Payment Modal
      $('.advanced_payment').click(function() {
              $(".payment_area").addClass("payment_modal_show");
              $(".payment_area_overlay").addClass("payment_modal_fadeIn");
          });
          $(".payment_toggle").click(function(e) {
              e.preventDefault();
              $(".payment_area").removeClass("payment_modal_show");
              $(".payment_area_overlay").removeClass("payment_modal_fadeIn");
          });


            //video popup
           $('.video-popup').click(function() {
              $(" .video_area").addClass("video_modal_show");
              $(".video_area_overlay").addClass("video_modal_fadeIn");
              $(".topbar_area,.menu_area, .mobile-menu").css("display","none");
             $(".banner_main").css({"position":"inherit","top":"inherit","transform":"inherit"});

          });
          $(".video_toggle").click(function(e) {
              e.preventDefault();
              $(".video_area").removeClass("video_modal_show");
              $(".video_area_overlay").removeClass("video_modal_fadeIn");
              $(".topbar_area,.menu_area,.mobile-menu").css("display","block");
              $(".banner_main").css({"position":"absolute","top":"50%","transform":"translateY(-50%)"});

          });




        //Blog tav
      $(".tabs-select.tabs-select-2").addClass("sign_active");
        $(".sign_in_default").show().siblings().hide();
        $(".acc").on("click", function() {
            var self = $(this);
            self.addClass("sign_active").siblings().removeClass("sign_active");
            var select = self.attr("data-tabs");
            $(".tabs-det-1[data-tabs-details="+select+"]").show().siblings().hide();
        });


        //Main slider
        $('.cta_slider').owlCarousel({
            items:1,
            loop:true,
            nav:true,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
            dots:true,
            autoplay:true,
            });


        // dropdown menu
        $(".nav_list li span, .menu_list_type li p, .login-reg-list li .profile-nav").click(function(e) {
            e.preventDefault();
            var thisItem = $(this);

            $(this).toggleClass('collapsed');

            if(thisItem.parent().hasClass("sel")) {
                thisItem.parent().find(".sub-menu, .sub_menu_list, .profile-nav-drop");
                thisItem.parent(".nav_list li.sel, .menu_list_type li.sel, .login-reg-list li.sel").removeClass("sel");
            }

            else {
                thisItem.parent().find(".sub-menu, .sub_menu_list, .profile-nav-drop").css({"pointer-events": "none"});
                thisItem.parent(".nav_list li.sel, .menu_list_type li.sel, .login-reg-list li.sel").removeClass("sel");

                if(thisItem.next(".sub-menu, .sub_menu_list, .profile-nav-drop").length) {
                    thisItem.parent().addClass("sel");
                    thisItem.next(".sub-menu, .sub_menu_list, .profile-nav-drop").css({"transform": "scale(1)", "display": "block", "pointer-events": "all"});
                }
            }
        });
        $(document).mouseup(function(e){
            var body = $(".sub-menu, .sub_menu_list, .profile-nav-drop");
            if(!body.is(e.body) && body.has(e.target).length === 0){
                body.css({"transform": "scale(0.8)", "display": "none", "transition": "all 0.3s ease-in-out"});
            }
        });
        $(document).mouseup(function(e){
            var selbody = $(".nav_list li.sel ,.sub_menu_list li.sel, .login-reg-list li.sel");
            if(!selbody.is(e.selbody) && selbody.has(e.target).length === 0){
                selbody.removeClass("sel");
            }
        });

        // Food items
        //Isotope Masonry 
		    var $grid = $('.all_food').isotope({
            itemSelector: '.grid-item',
          });	

        $('.food_menu').on( 'click', 'a', function() {
          var filterValue = $(this).attr('data-filter');
          $grid.isotope({ filter: filterValue });
        });
        
        $('.food_menu a').on('click', function(event) {
            $('.food_menu a').removeClass('active-food');
            $(this).addClass('active-food');
            event.preventDefault();
        });   

        //Magnific popup 
        $('.all_food').each(function() {
            $(this).magnificPopup({
                delegate: 'a', // 
                type: 'image',
                gallery: {
                  enabled:true
                }
            });
        });

      //Magnific popup 
      $('.gallery').each(function() {
          $(this).magnificPopup({
              delegate: 'a', // 
              type: 'image',
              gallery: {
                enabled:true
              }
          });
      });


       //Main slider
       $('.testimonial_slider').owlCarousel({
        items:1,
        loop:true,
        nav:true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        dots:true
        });

    //   Blog sidebar slider
      $('.blog_post_slider').owlCarousel({
        items:1,
        loop:true,
        nav:true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        dots:true,
        });


      // food slider
      $('.food_slider').owlCarousel({
        items:4,
        loop:true,
        nav:true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        dots:true,
        });

        //Blog comment 
        $(".leave_blog_reply").hide(); 
        $(".reply_btn").on("click", function() {
        $(this).siblings(".leave_blog_reply").show();
        }); 
        $(".cancel_btn").on("click", function()
        { $(this).parent().parent(".leave_blog_reply").hide(); 
        return false;
        });

    //food details
     var galleryThumbs = new Swiper('.gallery-thumbs', {
          spaceBetween: 10,
          slidesPerView: 4,
          loop: true,
          freeMode: true,
          loopedSlides: 5, //looped slides should be the same
          watchSlidesVisibility: true,
          watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
          spaceBetween: 10,
          loop:true,
          loopedSlides: 5, //looped slides should be the same
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          thumbs: {
            swiper: galleryThumbs,
          },
        });





      //Reservation

    });

//header
    $(document).on('scroll', function() {
        var scrollPos = $(this).scrollTop();

      if( scrollPos > 10 ) {
          $('.menu_area').addClass('navbar_home');
          $('.mobile-menu').addClass('mobile_navbar_home');
      }

      else {
          $('.menu_area').removeClass('navbar_home');
          $('.mobile-menu').removeClass('mobile_navbar_home');
      }
  });



})(jQuery);